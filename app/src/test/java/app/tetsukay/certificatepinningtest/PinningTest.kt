package app.tetsukay.certificatepinningtest

import com.taroid.knit.should
import junit.framework.Assert.fail
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import okhttp3.Request
import org.junit.Test
import javax.net.ssl.SSLPeerUnverifiedException

class PinningTest {
    /**
     * 有効なCertificate Pinning のテスト
     */
    @Test
    fun withValidPinner(): Unit = runBlocking {
        val job = GlobalScope.launch {
            val request = Request.Builder().url("https://www.tetsukay.app/").build()
            val pinner = CertificatePinner.Builder()
                    .add("www.tetsukay.app", "sha256/AShTllD7az9l8twvu2FdZZgTL0l3u7eJuitLi8GRh68=")
                    .build()
            val client = OkHttpClient.Builder().certificatePinner(pinner).build()
            client.newCall(request).execute().isSuccessful.should be true
        }
        job.join()
        return@runBlocking
    }

    /**
     * 有効なCA証明書(Let's Encrypt)の Certificate Pinning のテスト
     */
    @Test
    fun withValidCAPinner(): Unit = runBlocking {
        val job = GlobalScope.launch {
            val request = Request.Builder().url("https://www.tetsukay.app/").build()
            val pinner = CertificatePinner.Builder()
                    .add("www.tetsukay.app", "sha256/YLh1dUR9y6Kja30RrAn7JKnbQG/uEtLMkBgFF2Fuihg=")
                    .build()
            val client = OkHttpClient.Builder().certificatePinner(pinner).build()
            client.newCall(request).execute().isSuccessful.should be true
        }
        job.join()
        return@runBlocking
    }

    /**
     * 有効なCA証明書(Let's Encrypt)の Certificate Pinning のテスト（letsencrypt.orgドメイン）
     */
    @Test
    fun withValidCAPinnerOfLetsEncrypt(): Unit = runBlocking {
        val job = GlobalScope.launch {
            val request = Request.Builder().url("https://letsencrypt.org/").build()
            val pinner = CertificatePinner.Builder()
                    .add("letsencrypt.org", "sha256/YLh1dUR9y6Kja30RrAn7JKnbQG/uEtLMkBgFF2Fuihg=")
                    .build()
            val client = OkHttpClient.Builder().certificatePinner(pinner).build()
            client.newCall(request).execute().isSuccessful.should be true
        }
        job.join()
        return@runBlocking
    }

    /**
     * Certificate Pinning 無し
     */
    @Test
    fun withoutPinner(): Unit = runBlocking {
        val job = GlobalScope.launch {
            val request = Request.Builder().url("https://example.com/").build()
            val client = OkHttpClient.Builder().build()
            client.newCall(request).execute().isSuccessful.should be true
        }
        job.join()
        return@runBlocking
    }

    /**
     * 無効なCertificate Pinning のテスト
     */
    @Test
    fun withInvalidPinner(): Unit = runBlocking {
        val job = GlobalScope.launch {
            val request = Request.Builder().url("https://www.tetsukay.app/").build()
            val pinner = CertificatePinner.Builder()
                    .add("www.tetsukay.app", "sha256/1234567890ABCDEF=")
                    .build()
            val client = OkHttpClient.Builder().certificatePinner(pinner).build()

            try {
                client.newCall(request).execute()
                fail()
            } catch (e: SSLPeerUnverifiedException) {
                // OK
            }
        }
        job.join()
        return@runBlocking
    }

    /**
     * 異なる証明書のpinが指定された場合のテスト
     */
    @Test
    fun withInvalidPinner2(): Unit = runBlocking {
        val job = GlobalScope.launch {
            val request = Request.Builder().url("https://example.com/").build()
            val pinner = CertificatePinner.Builder()
                    .add("example.com", "sha256/AShTllD7az9l8twvu2FdZZgTL0l3u7eJuitLi8GRh68=")
                    .build()
            val client = OkHttpClient.Builder().certificatePinner(pinner).build()

            try {
                client.newCall(request).execute()
                fail()
            } catch (e: SSLPeerUnverifiedException) {
                // OK
            }
        }
        job.join()
        return@runBlocking
    }

}
